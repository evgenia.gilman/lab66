<?php
print "Please enter first string: \n";
$first_string = trim(fgets(STDIN));
print "Please enter second string: \n";
$second_string = trim(fgets(STDIN));

$short_line = [];
$long_line = [];

if(iconv_strlen($first_string) >= iconv_strlen($second_string)) {
    $long_line = str_split($first_string);
    $short_line = str_split($second_string);
} else {
    $long_line = str_split($second_string);
    $short_line = str_split($first_string);
}

$count = 0;

for($i = 0; $i < count($short_line); $i++) {
    if($short_line[$i] == $long_line[$i]) {
        $count++;
    } else{
        break;
    }
}

print "$count \n";