<?php

$goals_scored = ['1', '3', '4','2', '5', '2', '4','3', '1', '2', '3','7','6', '2', '5', '4', '2', '5','3', '5'];

$conceded_goals = ['1', '5', '7','4', '5', '0', '5','2', '0', '2', '5','3','2', '4', '5', '0', '2', '1','5', '7'];

for($i = 0; $i < count($goals_scored); $i++){
    if($goals_scored[$i] > $conceded_goals[$i]) {
        print "Win $goals_scored[$i]:$conceded_goals[$i] \n";
    } elseif($goals_scored[$i] < $conceded_goals[$i]) {
        print "Losing $goals_scored[$i]:$conceded_goals[$i] \n";
    } else {
        print "Draw $goals_scored[$i]:$conceded_goals[$i] \n";
    }
}
