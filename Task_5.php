<?php

print "Please enter number A: \n";
$num_a = trim(fgets(STDIN));
print "Please enter number B: \n";
$num_b = trim(fgets(STDIN));

$sum = 0;

for($i = 1; $i <= $num_b; $i++) {
    $sum = $sum + ($i * $num_a);
}

print "$sum \n";